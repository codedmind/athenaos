import os, sys
import importlib

HOME = os.path.expanduser("~")+"/"
ATHENAOS_PATH = HOME+".athena.d/"
sys.path.append(ATHENAOS_PATH+"lib/")
sys.path.append(ATHENAOS_PATH+"modules/")
import dearpygui.dearpygui as dpg
import toml
FILESYS = ATHENAOS_PATH+"files/"
VIEWPORT_WIDTH = 1200
VIEWPORT_HEIGHT = 600
WINDOWS = []
WINDOW_BAR_IDS = []
APPS = {}
LOG_BUFFER = []
DEBUG = True
BOT_NAME = "Athena"
CONFIG = {}

def load_config():
    global CONFIG
    with open(ATHENAOS_PATH+'config.toml', 'r+') as f:
        CONFIG = toml.load(f)

def save_config():
    global CONFIG
    with open(ATHENAOS_PATH+'config.toml', 'w+') as f:
        toml.dump(CONFIG, f)

def set_config(group, key, value, save_after = False):
    global CONFIG
    if CONFIG.get(group, "") == "":
        CONFIG[group] = {}
    CONFIG[group][key] = value
    save_config()
    
def get_config(group, key, default = None):
    global CONFIG
    if CONFIG.get(group, "") == "":
        CONFIG[group] = {}
    return CONFIG[group].get(key, default)

def echo(line, e_type = "echo"):
    if dpg.does_item_exist("log_window"): add_buffer(line, e_type)
    print(line)

def refresh_buffer_win():
    dpg.set_value("log_window", "-- BEGIN LOG --")
    for bf in LOG_BUFFER:
        dpg.set_value("log_window", bf['source']+": "+bf['text']+"\n"+dpg.get_value("log_window"))

def add_buffer(text, source = "system"):
    LOG_BUFFER.append({"text": text, "source": source})
    dpg.set_value("log_window", source+": "+text+"\n"+dpg.get_value("log_window"))

def close_win(w):
    if isinstance(w, Context): w = w.tag
    print(dpg.get_item_children(w))

    fully_destroy(w)

    if w in WINDOW_BAR_IDS: WINDOW_BAR_IDS.remove(w)
    if w in WINDOWS: WINDOWS.remove(w)
    refresh_win_bar()

def open_win(tag, on_close=close_win):
    if tag in WINDOWS:
        print("Window already created.")
        return

    with dpg.window(label=tag, tag=tag, on_close=lambda: on_close(Context(tag = tag))): pass
    WINDOWS.append(tag)
    refresh_win_bar()

def fully_destroy(name):
    sub = dpg.get_item_children(name)
    for item in sub:
        for c in sub[item]:
            if dpg.is_item_container(c):
                fully_destroy(c)
            if dpg.does_item_exist(c): dpg.delete_item(c)
    if dpg.does_item_exist(name): dpg.delete_item(name)

class Context:
    def __init__(self, tag = ""):
        self.ui = dpg
        self.tag = tag
        self.data = {}
        self.config = CONFIG

    def app_dir(self, name): 
        if not os.path.exists(ATHENAOS_PATH+"/apps/"+name+"/"):
            os.mkdir(ATHENAOS_PATH+"/apps/"+name+"/")
        return ATHENAOS_PATH+"/apps/"+name+"/"

    def load_textures(self, path): load_textures(path)
    def load_texture(self, path, name): load_texture(path, name)
    def say(self, txt): say(txt)
    def echo(self, txt, source="echo"): echo(txt, source)
    def fully_destroy(self, tag): fully_destroy(tag)
    def apps(self): return APPS
    def files_path(self): return FILESYS
    def app_path(self): return ATHENAOS_PATH
    def windows(self): return WINDOWS 
    def window_bar(self): return WINDOW_BAR_IDS
    def open_win(self, id, on_close = close_win): return open_win(id, on_close)
    def close_win(self, id): return close_win(id)

    def get_config(self, group, key, val = None): return get_config(group, key, val)
    def set_config(self, group, key, val, do_save = False): set_config(group, key, val, do_save)
    def save_config(self): save_config()

dpg.create_context()
dpg.create_viewport(title='Athena OS', width=VIEWPORT_WIDTH, height=VIEWPORT_HEIGHT)
dpg.setup_dearpygui()

def load_texture(tex_path, name):
    echo(f"Loading {tex_path}", "static_texture")
    width, height, channels, data = dpg.load_image(tex_path)
    dpg.add_static_texture(width, height, data, tag=name)

def load_textures(tex_dir):
    echo("Scanning "+tex_dir, "static_texture")
    for file in os.listdir(tex_dir):
        filename = os.fsdecode(file)
        if os.path.isdir(tex_dir+filename):
            load_textures(tex_dir+filename+"/")
        else:
            load_texture(tex_dir+filename, filename.split(".")[0])

def load_app(modname):
    APPS[modname] = {
        "meta": {},
        "active": True
    }
    f = importlib.import_module("apps."+modname)
    APPS[modname]["meta"] = f.app_data()
    APPS[modname]["active"] = True
    if f.app_data().get("on_open", None) != None:
        APPS[modname]["on_open"] = f.app_data()["on_open"]
    else:
        if hasattr(f, "on_open"):
            APPS[modname]["on_open"] = f.on_open
        else:
            print("No entrypoint for application",modname,"found, treating as library.")
            APPS[modname]["active"] = False

    if hasattr(f, "on_load"):
        f.on_load(Context(tag=modname))
    elif f.app_data().get("on_load", ""): 
        f.app_data()["on_load"](Context(tag=modname))

    if APPS[modname]["active"]:
        if APPS[modname]["meta"].get("group", "") != "":
            if not dpg.does_item_exist("apps_"+APPS[modname]["meta"]["group"]):
                dpg.add_menu(label=APPS[modname]["meta"]["group"], parent="menu_apps", tag="apps_"+APPS[modname]["meta"]["group"])

            dpg.add_menu_item(label=modname, callback=lambda: APPS[modname]["on_open"](Context(tag = modname)), parent="apps_"+APPS[modname]["meta"]["group"], tag="menu_"+modname)
        else:
            dpg.add_menu_item(label=modname, callback=lambda: APPS[modname]["on_open"](Context(tag = modname)), parent="menu_apps", tag="menu_"+modname)
        #dpg.set_item_user_data("menu_"+modname, Context())

def load_apps():
    APPS.clear()
    for file in os.listdir(ATHENAOS_PATH+"apps/"):
        filename = os.fsdecode(file)
        if filename.endswith(".py"):
            modname = filename.split(".")[0]
            load_app(modname)

def default_conf():
    set_config("internal", "autostart", ["calculator"])
    set_config("internal", "name", "Athena")
    set_config("internal", "user", "User", True)

def do_autostart():
    ls = get_config("internal", "autostart", [])
    for aname in ls:
        if APPS.get(aname):
            print(APPS[aname])
            APPS[aname]["on_open"](Context(tag=aname))

def setup():
    with dpg.texture_registry(show=False): load_textures(ATHENAOS_PATH+"img/")

    with dpg.viewport_menu_bar():
        with dpg.menu(label="File", tag="menu_file"):
            dpg.add_menu_item(label="Eval", callback=lambda: open_eval_win())
            dpg.add_menu_item(label="Default", callback=lambda: default_conf())

        with dpg.menu(label="Apps", tag="menu_apps"): pass
        with dpg.menu(label="Help", tag="menu_help"): 
            dpg.add_menu_item(label="Help browser", callback=open_help)

    with dpg.window(label="Windows", pos=(0, 30), tag="window_bar_host", no_close=True):
        dpg.add_group(tag="window_bar")

    with dpg.handler_registry(): dpg.add_mouse_drag_handler(callback=evalcb)
            
    with dpg.handler_registry(): dpg.add_key_press_handler(callback=handle_key_input)

def activated_window_button(w):
    print(w)
    lbl = dpg.get_item_label(w)
    dpg.configure_item(lbl, show=not dpg.get_item_configuration(lbl)["show"])

def refresh_win_bar():
    for id in WINDOW_BAR_IDS: dpg.delete_item(id)

    WINDOW_BAR_IDS.clear()

    for win in WINDOWS:
        b = dpg.add_button(label=win, callback=activated_window_button, parent="window_bar")
        WINDOW_BAR_IDS.append(b)

def evalcb(w, i, n):
    pass

HELP_D = {
    "Main": "This is the main help heading.",
    "Keyboard": """ = Keyboard = 
F1 = Help
F2 = Evaluator
F3 = Focus on terminal
    """,
    "Evaluator": """ = Evaluator =
This function allows you to run arbitrary code in the environment. 
The scope is given access to a few variables;
"ui" = The core of the UI framework, has access to all functions related to the interface.
"ctx" = A Context object, has access to various utility functions, configs, etc.
"cfg" = A direct link to the CONFIG object.
"writeln" = Writes to the Terminal log.

F5 = Execute

    """
}

def open_help(subheading = None):
    open_win("help menu")
    
    with dpg.group(parent="help menu", horizontal=True):
        dpg.add_group(tag="help_nav")
        dpg.add_input_text(tag="help_text", width=300, height=400, multiline=True)

    def set_help(w):
        lbl = dpg.get_item_label(w)
        dpg.set_value("help_text", HELP_D[lbl])

    def set_help_app(w):
        lbl = dpg.get_item_label(w)
        dpg.set_value("help_text", APPS[lbl]["meta"]["help"])

    for heading, body in HELP_D.items():
        dpg.add_button(label = heading, parent="help_nav", callback=set_help, width=100)

    first = True
    for appname in APPS:
        app = APPS[appname]
        if app["meta"].get("help", "") != "":
            if first:
                first = False
                dpg.add_text(" --- Apps ---", parent="help_nav")
            dpg.add_button(label = appname, parent="help_nav", callback=set_help_app, width=100)

def handle_key_input(w, key):
    # TODO
    # at some point, shift this in to a dict of keys and functions for easier extending later
    #echo(str(key), "input")
    if key == 292: # space, enter
        dpg.focus_item("terminal_input")

    elif key == 257 and dpg.is_item_focused("terminal_input"):
        handle_command()

    elif key == 294: #F5
        if dpg.does_item_exist("eval_body"):
            if dpg.is_item_focused("eval_body") or dpg.is_item_focused("evaluator"):
                code = dpg.get_value("eval_body")
                st = {
                    "ui": dpg,
                    "ctx": Context(),
                    "cfg": CONFIG,
                    "writeln": echo
                }
                eval(code, st)

    elif key == 290:
        if "help menu" not in WINDOWS:
            open_help()

    elif key == 291:
        open_eval_win()
            

def open_eval_win():
    if "evaluator" not in WINDOWS:
        open_win("evaluator")
        with dpg.group(horizontal=True, parent="evaluator"):
            dpg.add_button(label="Eval Select", callback=lambda: print(dpg.get_value("eval_body")))
            dpg.add_button(label="Eval All (F5)", callback=lambda: print(dpg.get_item_state("eval_body")))
            dpg.add_button(label="Save", callback=lambda: print("lol"))
            dpg.add_button(label="Open", callback=lambda: print("lol"))



        dpg.add_input_text(tag="eval_body", width=300, height=400, tab_input=True, multiline=True, callback=evalcb, parent="evaluator")
def handle_command(*args):
    text = dpg.get_value("terminal_input")
    if(text != ""):
        dpg.set_value("terminal_input", "")
        add_buffer(text, "YOU")
    dpg.focus_item("terminal_input")
    
    


def say(txt):
    add_buffer(txt, BOT_NAME)

def handle_listen(*args):
    print("Not yet implemented.")

with dpg.window(label="Universal Terminal", pos=(400, 200), no_close=True):
    with dpg.group(horizontal=True):
        dpg.add_input_text(tag="terminal_input", width=500)#, callback=handle_command, on_enter=True)
        dpg.add_button(label="SEND", callback=handle_command)
        dpg.add_button(label="LSTN", callback=handle_listen)
    dpg.add_input_text(tag="log_window", width=500, height=400, multiline=True)
    dpg.set_value("log_window", "-- LOG START --")

if __name__ == "__main__":
    load_config()
    setup()
    load_apps()
    do_autostart()
    dpg.show_viewport()
    dpg.start_dearpygui()
    dpg.destroy_context()