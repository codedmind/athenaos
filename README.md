# AthenaOS
AOS is a planned virtual environment and desktop assistant.
It will contain an interactive AI assistant that can be communicated with via text or voice, alongside a virtual micro-OS which will contain it's own highly extendible environment and applications which interact with the AI.

# TODO
- [ ] Log window
    - [ ] System logs
    - [ ] Chat from the AI
    - [ ] Toggle for filtering each source
- [ ] Configs
- [ ] Apps (Not a complete list, just ideas as they come)
    - [ ] Dice roll
	- [ ] echo results
    - [ ] Deck of cards simulator
        - [ ] Deck window
        - [ ] Field window for generic storage
        - [ ] Hand window
    - [ ] Help screen
    - [ ] Notes app
    - [ ] Journal
    - [ ] IRC/chat client
    - [ ] http client (for sending data to webhooks/tests)
    - [ ] File browser
    - [ ] Calculator
    - [ ] Timer
    - [ ] Clock
    - [ ] Wikipedia/Summary viewer
    - [ ] news (newsapi)
    - [ ] Weather
    - [ ] Wolfram API's
    - [ ] Search engine results viewer
    - [ ] Podcast/RSS feed reader
    - [ ] Youtube grabber/player
    - [ ] Internet radio
    - [ ] LibreTranslator
        - [ ] Add feature to remember last-used language settings
    - [ ] Pokedex
    - [ ] themoviedb
    - [ ] ddg instant answer list
- [ ] Autostarting apps
- [ ] Conversational AI
    - [ ] Rivescript?
    - [ ] Commands
        - [ ] Launch <app name>
        - [ ] Hide [window] [<app name>]
    - [ ] Startup list
        - [ ] Sends list of inputs to the AI to run at startup
- [ ] Support for apps registering triggers for the AI
    - [ ] For example, journal app adds a trigger that lets you say `today i went to the park` which would open the journal, adding a new entry

# Extensions
## Applications
Custom apps in the environment are defined as .py scripts in the apps directory.
```py
# notepad.py


def app_data():
    return {
    # This is metadata that may or may not be used for information in the environment
    "name": "notepad",
    "author": "Kaiser",
    "version": "0",

    # Not yet implemented
    # Will be a list of tags to tell the environment what features this app uses
    "features": [],

    ## open refers to the app being spawned in the interface
    ## if an `on_open` value is not defined pointing to a valid function, interface tries to detect one called `on_open`
    "on_open": open_apps
}
## Flow for creating a window in an application
## Because we're using pygui as the environment, allowing extensions to maniplate the UI is very easy
## Everything is done by targetting ID's, you can add, edit, modify any window that you know the tag of
## and all functionality from the frameworks core is exposed via the single `ctx.ui` variable
def open_apps(ctx):
    ## Check if the name you're assigning is open already
    if "notepad" not in ctx.windows():
        ## If not, call open_win which creates an empty window for the application with the given ID
        ## open_win takes an optional callable second argument for a callback to use when the window is closed
        ## can be used for cleaning up any external data
        ctx.open_win("notepad")

        ## Then you can alter the window
        ctx.ui.add_button(label="Notepad", callback=lambda: print("Tested"), parent="notepad")
```
