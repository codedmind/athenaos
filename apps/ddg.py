## NOTE
## Apps format
## open refers to the app being spawned in the interface
## if an `open` value is not defined pointing to a valid function, interface tries to detect one called `open_app`
def app_data():
    return {
    "name": "ddg",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "net"
}
import requests

def query_ddg(ctx):
    query = ctx.ui.get_value("ddg_query")
    if(query != ""):
        res = requests.get(f"https://api.duckduckgo.com/?q={query}&format=json&pretty=1").json()
        print(res)
        if ctx.ui.does_item_exist("ddg_result"):
            ctx.fully_destroy("ddg_result")

        with ctx.ui.group(parent="ddg"):
            ctx.ui.add_text(res["Abstract"])
def on_open(ctx):
    if "ddg" not in ctx.windows():
        ctx.open_win("ddg")
        with ctx.ui.group(parent="ddg", horizontal=True):
            ctx.ui.add_input_text(tag="ddg_query", parent="ddg", width=200)
            ctx.ui.add_button(label="Query", callback=lambda: query_ddg(ctx), parent="ddg")
