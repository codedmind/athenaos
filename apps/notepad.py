## NOTE
## Apps format
## open refers to the app being spawned in the interface
## if an `open` value is not defined pointing to a valid function, interface tries to detect one called `open_app`
def app_data():
    return {
    "name": "notepad",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "system",
    "on_open": open_apps
}

def open_apps(ctx):
    if "notepad" not in ctx.windows():
        ctx.open_win("notepad")
        with ctx.ui.group(horizontal=True, parent="notepad"):
            ctx.ui.add_button(label="Save", callback=lambda: print("Tested"))
            ctx.ui.add_button(label="Load", callback=lambda: print("Tested"))
        ctx.ui.add_input_text(tag="notepad_body", width=300, height=400, tab_input=True, multiline=True, parent="notepad")