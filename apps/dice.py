## NOTE
## Apps format
## open refers to the app being spawned in the interface
## if an `open` value is not defined pointing to a valid function, interface tries to detect one called `open_app`
def app_data():
    return {
    "name": "dice",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "utility"
}
import random

def roll_dice(ctx):
    ctx.ui.delete_item("dice_roll")
    r = random.randint(1, 6)
    ctx.ui.add_image(f"dice_{r}", parent="dice", tag="dice_roll")
    
def on_open(ctx):
    if "dice" not in ctx.windows():
        ctx.open_win("dice")
        ctx.ui.add_button(label="Roll", callback=lambda: roll_dice(ctx), parent="dice")
        ctx.ui.add_image("dice_1", parent="dice", tag="dice_roll")