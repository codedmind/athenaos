## NOTE
## Apps format
## open refers to the app being spawned in the interface
## if an `open` value is not defined pointing to a valid function, interface tries to detect one called `open_app`
def app_data():
    return {
    "name": "settings",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "system",
    "on_open": open_apps
}

def custom_close(ctx):
    print("CLOSING SETTINGS")
    ctx.close_win("settings")

def open_apps(ctx):
    if "settings" not in ctx.windows():
        ctx.open_win("settings", custom_close)
        ctx.ui.add_button(label="settings", callback=lambda: custom_close(ctx), parent="settings")
        ctx.ui.add_image("no_mate", parent="settings")