import os

def app_data():
    return {
    "name": "bestiary",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "utility",
    #"on_open": open_apps,
    #"on_install": check_dir
}

def on_install(ctx): 
    f = ctx.app_dir(app_data()["name"])
    if not os.path.exists(f+"data.json"):
        with open(f+"data.json", 'w+') as f:
            f.write("{}")

def add_row(ctx, id):
    with ctx.ui.group(horizontal=True, parent="bestiary", tag=f"bestiary_{id}"):
        ctx.ui.add_input_text(tag=f"bestiary_key_{id}", hint="Key", width=100)
        ctx.ui.add_input_text(tag=f"bestiary_value_{id}", hint="Value", width=250)
        ctx.ui.add_button(label=f"Remove {id}")

def prompt_row(ctx):
    with ctx.ui.window(tag="prompt", modal=True):
        def cb(w):
            add_row(ctx, ctx.ui.get_value("prompt_input"))
            ctx.ui.delete_item("prompt_input")
            ctx.ui.delete_item("prompt_button")
            ctx.ui.delete_item("prompt")
        ctx.ui.add_input_text(tag="prompt_input")
        ctx.ui.add_button(label="add", callback=cb, tag="prompt_button")
        ctx.ui.focus_item("prompt_input")

def on_open(ctx):
    if "bestiary" not in ctx.windows():
        ctx.open_win("bestiary")
        ctx.ui.set_item_width("bestiary", 500)
        ctx.ui.set_item_height("bestiary", 400)
        with ctx.ui.group(horizontal=True, parent="bestiary"):
            ctx.ui.add_button(label="+", callback=lambda: prompt_row(ctx))
            ctx.ui.add_button(label="Save", callback=lambda: print("Tested"))
            ctx.ui.add_button(label="Open", callback=lambda: print("Tested"))
