def app_data():
    return {
    "name": "calculator",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "utility",
    "help": "A calculator."
}

import ast
import operator as op

# supported operators
operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
             ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor,
             ast.USub: op.neg}

def eval_expr(expr):
    """
    >>> eval_expr('2^6')
    4
    >>> eval_expr('2**6')
    64
    >>> eval_expr('1 + 2*3**(4^5) / (6 + -7)')
    -5.0
    """
    return eval_(ast.parse(expr, mode='eval').body)

def eval_(node):
    if isinstance(node, ast.Num): # <number>
        return node.n
    elif isinstance(node, ast.BinOp): # <left> <operator> <right>
        return operators[type(node.op)](eval_(node.left), eval_(node.right))
    elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
        return operators[type(node.op)](eval_(node.operand))
    else:
        raise TypeError(node)

def append_expr(ctx, expr):
    ctx.ui.set_value("calc_expr", ctx.ui.get_value("calc_expr")+expr)

def bkspc(ctx):
    ctx.ui.set_value("calc_expr", ctx.ui.get_value("calc_expr")[:-1]) 

def run_expr(ctx):
    ret = eval_expr(ctx.ui.get_value("calc_expr"))
    ctx.ui.set_value("calc_expr", str(ret))

def clear_expr(ctx):
    ctx.ui.set_value("calc_expr", "")

def on_open(ctx):
    if "calculator" not in ctx.windows():
        ctx.open_win("calculator")
        ctx.ui.add_input_text(parent="calculator", tag="calc_expr", width=150)
        with ctx.ui.group(parent="calculator", horizontal=True):
            ctx.ui.add_button(label="7", callback=lambda: append_expr(ctx, "7"), width=40, height=40)
            ctx.ui.add_button(label="8", callback=lambda: append_expr(ctx, "8"), width=40, height=40)
            ctx.ui.add_button(label="9", callback=lambda: append_expr(ctx, "9"), width=40, height=40)
            ctx.ui.add_button(label="D", callback=lambda: bkspc(ctx), width=20, height=40)
            ctx.ui.add_button(label="C", callback=lambda: clear_expr(ctx), width=20, height=40)
        with ctx.ui.group(parent="calculator", horizontal=True):
            ctx.ui.add_button(label="4", callback=lambda: append_expr(ctx, "4"), width=40, height=40)
            ctx.ui.add_button(label="5", callback=lambda: append_expr(ctx, "5"), width=40, height=40)
            ctx.ui.add_button(label="6", callback=lambda: append_expr(ctx, "6"), width=40, height=40)
            ctx.ui.add_button(label="/", callback=lambda: append_expr(ctx, "/"), width=40, height=40)
        with ctx.ui.group(parent="calculator", horizontal=True):
            ctx.ui.add_button(label="1", callback=lambda: append_expr(ctx, "1"), width=40, height=40)
            ctx.ui.add_button(label="2", callback=lambda: append_expr(ctx, "2"), width=40, height=40)
            ctx.ui.add_button(label="3", callback=lambda: append_expr(ctx, "3"), width=40, height=40)
            ctx.ui.add_button(label="*", callback=lambda: append_expr(ctx, "*"), width=40, height=40)
        with ctx.ui.group(parent="calculator", horizontal=True):
            ctx.ui.add_button(label="=", callback=lambda: run_expr(ctx), width=30, height=40)
            ctx.ui.add_button(label="+", callback=lambda: append_expr(ctx, "+"), width=30, height=40)
            ctx.ui.add_button(label="-", callback=lambda: append_expr(ctx, "-"), width=30, height=40)
            ctx.ui.add_button(label="(", callback=lambda: append_expr(ctx, "("), width=30, height=40)
            ctx.ui.add_button(label=")", callback=lambda: append_expr(ctx, "_"), width=30, height=40)
            #ctx.ui.add_button(label="C", callback=lambda: append_expr(ctx, "C"))