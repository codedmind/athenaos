
def app_data():
    return {
    "name": "paint",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "on_open": open_apps
}

def open_apps(ctx):
    if "paint" not in ctx.windows():
        ctx.open_win("paint")
        ctx.ui.add_button(label="Paint", callback=lambda: print("Tested paint"), parent="paint")