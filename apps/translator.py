
def app_data():
    return {
    "name": "translator",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "help": "Powered by Libre Translate."
}

import requests
def on_install(ctx):
    cur_mirror = ""
    if ctx.get_config("translator", "mirror", "") == "":
        ctx.set_config("translator", "mirror", "https://translate.astian.org", True)

    cur_mirror = ctx.get_config("translator", "mirror")

    if ctx.get_config("translator", "languages", []) == []:
        data = requests.get(cur_mirror+"/languages").json()
        ctx.set_config("translator", "languages", data, True)
        
import json
def send_translate(ctx):
    cur_mirror = ctx.get_config("translator", "mirror")
    sourcelang = ctx.ui.get_value("translator_src").split("|")[0]
    targetlang = ctx.ui.get_value("translator_trg").split("|")[0]
    sourcetext = ctx.ui.get_value("translator_text_source")

    print(sourcelang,targetlang,sourcetext)
    res = requests.post(cur_mirror+"/translate", 
    headers = { "Content-Type": "application/json" },
    data = json.dumps({
		"q": sourcetext,
		"source": sourcelang,
		"target": targetlang,
		"format": "text"
    })
    ).json()
    ctx.ui.set_value("translator_text_target",res["translatedText"])

def pretty_langs(ctx):
    out = []
    cur = ctx.get_config("translator", "languages", [])
    for lang in cur:
        out.append(lang["code"]+"|"+lang["name"])

    return out

def on_open(ctx):
    if "translator" not in ctx.windows():
        languages = pretty_langs(ctx)
        src_default = "en|English"
        target_default = "de|German"
        ctx.open_win("translator")
        ctx.ui.set_item_width("translator", 640)
        ctx.ui.set_item_height("translator", 400)
        with ctx.ui.group(parent="translator", horizontal=True):
            ctx.ui.add_combo(languages, label="Source", tag="translator_src", width=250, default_value=src_default)
            ctx.ui.add_combo(languages, label="Target", tag="translator_trg", width=250, default_value=target_default)

        with ctx.ui.group(parent="translator", horizontal=True):
            ctx.ui.add_input_text(hint="Text to translate", tag="translator_text_source", multiline=True, width=300, height=300)
            ctx.ui.set_value("translator_text_source", "hello world")
            ctx.ui.add_input_text(hint="Result", tag="translator_text_target", multiline=True, width=300, height=300)
        
        ctx.ui.add_button(label="Send", callback=lambda: send_translate(ctx), parent="translator")