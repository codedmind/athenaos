## NOTE
## Apps format
## open refers to the app being spawned in the interface
## if an `open` value is not defined pointing to a valid function, interface tries to detect one called `open_app`
def app_data():
    return {
    "name": "weather",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "group": "net"
}
import requests, os

def get_weather(ctx):
    apikey = ctx.get_config("keys", "weather", "")
    loc = ctx.ui.get_value("weather_input")

    if loc == "":
        ctx.say("No location given.")
        return

    ctx.set_config("weather", "location", loc)
    if apikey == "":
        ctx.say("No apikey for weather found.")
        return

    if ctx.ui.does_item_exist("weather_result"):
        ctx.fully_destroy("weather_result")

    res = requests.get(f"http://api.weatherapi.com/v1/current.json?key={apikey}&q={loc}&aqi=no").json()
    print(res)

    with ctx.ui.group(parent="weather", tag="weather_result", horizontal=False):
        w_icon = "https:"+res["current"]["condition"]["icon"]
        name = "_".join(w_icon.split("/")[-2:])
        print(name)
        if not os.path.exists(ctx.app_path()+"img/downloads/"+name):
            ctx.say("Downloading icon from server...")
            with open(ctx.app_path()+"img/downloads/"+name, "wb+") as f:
                f.write(requests.get(w_icon).content)
        
        with ctx.ui.texture_registry(show=False):
            ctx.load_texture(ctx.app_path()+"img/downloads/"+name, name)

        cw = res["current"]
        loc = res["location"]
        ctx.ui.add_image(name, parent="weather_result")  
        ctx.ui.add_text(f"It is a {cw['condition']['text']} {cw['temp_c']}c / {cw['temp_f']}f.")
        ctx.ui.add_text(f"Weather report for {loc['name']} in {loc['region']}. {loc['country']}.")

def on_open(ctx):
    loc = ctx.get_config("weather", "location", "")
    if "weather" not in ctx.windows():
        ctx.open_win("weather")
        ctx.ui.set_item_width("weather", 300)
        ctx.ui.set_item_height("weather", 300)
        with ctx.ui.group(parent="weather", horizontal=True):
            ctx.ui.add_input_text(hint="Location", tag="weather_input")
            ctx.ui.add_button(label="search", callback=lambda: get_weather(ctx))
        ctx.ui.set_value("weather_input", loc)