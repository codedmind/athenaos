
def app_data():
    return {
    "name": "autostart",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "on_open": open_apps
}
storage = []

def on_load(ctx):
    global storage
    storage = ctx.get_config("internal", "autostart", [])

def save_as(ctx):
    global storage
    ctx.set_config("internal", "autostart", storage)

def delete_as(w, _, ctx):
    global storage
    item = w[8:]
    print(item)
    storage.remove(item)
    ctx.fully_destroy("asg_"+item)
    save_as(ctx)

def add_as(w, _, ctx):
    global storage
    item = ctx.ui.get_value("new_as")
    ctx.ui.set_value("new_as", "")

    print(item)
    storage.append(item)
    with ctx.ui.group(parent="autostart",horizontal = True, tag = "asg_"+item):
        ctx.ui.add_input_text(tag = "as_"+item)
        ctx.ui.set_value("as_"+item, item)
        ctx.ui.add_button(tag = "destroy_"+item, callback=delete_as, user_data = ctx, label = "-")
    save_as(ctx)
def open_apps(ctx):
    global storage
    if "autostart" not in ctx.windows():
        ctx.open_win("autostart")
        with ctx.ui.group(parent="autostart", horizontal = True):
            ctx.ui.add_input_text(tag = "new_as", hint = "App name")
            ctx.ui.add_button(callback = add_as, user_data = ctx, label = "+")

        for item in storage:
            with ctx.ui.group(parent="autostart",horizontal = True, tag = "asg_"+item):
                ctx.ui.add_input_text(tag = "as_"+item)
                ctx.ui.set_value("as_"+item, item)
                ctx.ui.add_button(tag = "destroy_"+item, callback=delete_as, user_data = ctx, label = "-")
                

            