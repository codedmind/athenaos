
def app_data():
    return {
    "name": "clock",
    "author": "Kaiser",
    "version": "0",
    "features": [],
    "on_open": open_apps
}

def open_apps(ctx):
    if "clock" not in ctx.windows():
        ctx.open_win("clock")
        